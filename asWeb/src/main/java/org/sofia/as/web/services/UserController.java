package org.sofia.as.web.services;

import java.util.List;

import org.sofia.as.objectDomain.User;
import org.sofia.as.services.IUserService;
import org.sofia.as.services.admin.IGenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private IUserService userService;

	@RequestMapping(value = "getUsers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<User>> getUsers(@RequestParam Integer idApp)
			throws Exception {

		ResponseEntity<List<User>> response = null;
		try {
			List<User> users = userService.getUsers(idApp);
			response = new ResponseEntity<List<User>>(users, HttpStatus.OK);
		} catch (Exception e) {
			throw e;
		}

		return response;
	}

	@RequestMapping(value = "getAdvisors", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<User>> getAdvisors() throws Exception {

		ResponseEntity<List<User>> response = null;
		try {
			List<User> users = userService.getAdvisors();
			response = new ResponseEntity<List<User>>(users, HttpStatus.OK);
		} catch (Exception e) {
			throw e;
		}

		return response;
	}

	@RequestMapping(value = "addUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> addUser(@RequestBody User user)
			throws Exception {

		ResponseEntity<User> response = null;
		try {
			User result = userService.addUser(user);
			response = new ResponseEntity<User>(result, HttpStatus.OK);
		} catch (Exception e) {
			throw e;
		}

		return response;
	}

	@RequestMapping(value = "updateUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> updateUser(@RequestBody User user)
			throws Exception {

		ResponseEntity<User> response = null;
		try {
			userService.updateUser(user, true);
			response = new ResponseEntity<User>(new User(), HttpStatus.OK);
		} catch (Exception e) {
			throw e;
		}

		return response;
	}

	@RequestMapping(value = "verifyCode", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public void verifyCode(@RequestParam String code) throws Exception {

		try {
			userService.verifyCode(code);
		} catch (Exception e) {
			throw e;
		}
	}

	@RequestMapping(value = "activate", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public void activate(@RequestParam String pwPrincipal,
			@RequestParam String pwTemp, @RequestParam String nbEmail)
			throws Exception {

		try {
			userService.activate(pwPrincipal, pwTemp, nbEmail);
		} catch (Exception e) {
			throw e;
		}
	}

	@RequestMapping(value = "validate", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public User validate(@RequestParam String pwPrincipal,
			@RequestParam String nbEmail) throws Exception {
		try {
			User user = userService.validate(pwPrincipal, nbEmail);
			return user;
		} catch (Exception e) {
			throw e;
		}
	}

	@RequestMapping(value = "recoverUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public void recoverUser(@RequestParam String nbEmail) throws Exception {
		try {
			userService.recoverUser(nbEmail);
		} catch (Exception e) {
			throw e;
		}
	}

}