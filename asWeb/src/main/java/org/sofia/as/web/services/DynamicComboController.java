package org.sofia.as.web.services;

import java.util.List;

import org.sofia.as.objectDomain.DynamicCat;
import org.sofia.as.services.admin.IGenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin")
public class DynamicComboController {

	@Autowired
	private IGenericService genericService;

	@RequestMapping(value = "loadCombo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Object>> loadObjects(
			@RequestBody DynamicCat dynamicCat) throws Exception {

		List<Object> objects = null;
		ResponseEntity<List<Object>> response = null;
		try {
			objects = genericService.findAllComnboValues(dynamicCat);
			response = new ResponseEntity<List<Object>>(objects, HttpStatus.OK);
		} catch (Exception e) {
			throw e;
		}

		return response;
	}

}