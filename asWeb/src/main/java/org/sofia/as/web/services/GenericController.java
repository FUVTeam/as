package org.sofia.as.web.services;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sofia.as.objectDomain.DynamicCat;
import org.sofia.as.objectDomain.Function;
import org.sofia.as.objectDomain.Profile;
import org.sofia.as.objectDomain.ProfileFunction;
import org.sofia.as.objectDomain.ProfileFunctionId;
import org.sofia.as.services.admin.IGenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping(value = "/admin")
public class GenericController {

	@Autowired
	private IGenericService genericService;

	@RequestMapping(value = "loadObjects", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Object>> loadObjects(
			@RequestBody DynamicCat dynamicCat) throws Exception {

		List<Object> objects = null;
		ResponseEntity<List<Object>> response = null;
		try {
			objects = genericService.findAll(dynamicCat.getObject());

			if (dynamicCat.getObject().equals(
					"org.sofia.as.objectDomain.Profile")) {
				List<Object> profiles = new ArrayList<Object>();
				for (Object o : objects) {
					Profile p = (Profile) o;

					List<Function> functions = new ArrayList<Function>();
					for (ProfileFunction f : p.getProfileFunctions()) {
						functions.add(f.getFunction());
					}

					p.setFunctions(functions);
					p.setProfileFunctions(null);
					profiles.add(p);
				}

				response = new ResponseEntity<List<Object>>(profiles,
						HttpStatus.OK);
			} else {
				response = new ResponseEntity<List<Object>>(objects,
						HttpStatus.OK);
			}
		} catch (Exception e) {
			throw e;
		}

		return response;
	}

	@RequestMapping(value = "saveObject", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> saveObject(@RequestBody String jsonObject)
			throws Exception {

		ResponseEntity<Object> response = null;
		try {
			Object object = parseObject(jsonObject);
			Object result = genericService.save(object);
			response = new ResponseEntity<Object>(result, HttpStatus.OK);
			return response;
		} catch (Exception e) {
			throw e;
		}

	}

	@RequestMapping(value = "deleteObject", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DynamicCat> deleteObject(
			@RequestBody DynamicCat dynamicCat) throws Exception {
		ResponseEntity<DynamicCat> response = null;
		try {
			response = new ResponseEntity<DynamicCat>(new DynamicCat(),
					HttpStatus.OK);
			return response;
		} catch (Exception e) {
			throw e;
		}
	}

	private Object parseObject(String jsonObject) {

		Object object = null;
		try {

			Map<String, Object> map = new HashMap<String, Object>();
			ObjectMapper mapper = new ObjectMapper();
			map = mapper.readValue(jsonObject,
					new TypeReference<HashMap<String, Object>>() {
					});

			Class<?> clazz = Class.forName((String) map.get("object"));
			object = clazz.newInstance();

			Map<String, Object> data = (Map<String, Object>) map.get("data");

			for (String key : data.keySet()) {

				String methodName = "set" + key.substring(0, 1).toUpperCase()
						+ key.substring(1);
				for (Method method : clazz.getDeclaredMethods()) {

					if (method.getName().startsWith(methodName)) {

						Object value = null;
						Type[] typeVariable = method.getGenericParameterTypes();
						if (typeVariable[0].equals(Integer.class)) {
							if (data.get(key) != null) {
								if (!data.get(key).toString().equals("")) {
									value = Integer.valueOf(data.get(key)
											.toString());
								}
							}
						} else if (typeVariable[0].equals(String.class)) {
							value = data.get(key).toString();
						}

						method.invoke(object, value);
						break;
					} else if (method.getName().startsWith("setTmAdd")) {
						method.invoke(object, new Date());
					}
				}
			}

			if (object instanceof Profile) {
				ArrayList<String> functions = (ArrayList<String>) data
						.get("functions");
				for (String f : functions) {
					ProfileFunction profileFunction = new ProfileFunction();
					profileFunction.setId(new ProfileFunctionId(
							((Profile) object).getIdProfile(), Integer
									.parseInt(f)));
					((Profile) object).getProfileFunctions().add(
							profileFunction);
				}
			}

			return object;

		} catch (Exception e) {
			return null;
		}
	}
}