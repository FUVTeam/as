package org.sofia.as.objectDomain;
// default package

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * App entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "as006_app", catalog = "mjas")
public class App implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -3969814089952698551L;
	private Integer idApp;
	private String nbApp;
	private String txDescription;
	private String txAccessPath;
	private Set<AppProfile> appProfiles = new HashSet<AppProfile>(0);

	// Constructors

	/** default constructor */
	public App() {
	}

	/** full constructor */
	public App(String nbApp, String txDescription, String txAccessPath,
			Set<AppProfile> appProfiles) {
		this.nbApp = nbApp;
		this.txDescription = txDescription;
		this.txAccessPath = txAccessPath;
		this.appProfiles = appProfiles;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id_app", unique = true, nullable = false)
	public Integer getIdApp() {
		return this.idApp;
	}

	public void setIdApp(Integer idApp) {
		this.idApp = idApp;
	}

	@Column(name = "nb_app", length = 45)
	public String getNbApp() {
		return this.nbApp;
	}

	public void setNbApp(String nbApp) {
		this.nbApp = nbApp;
	}

	@Column(name = "tx_description", length = 200)
	public String getTxDescription() {
		return this.txDescription;
	}

	public void setTxDescription(String txDescription) {
		this.txDescription = txDescription;
	}

	@Column(name = "tx_access_path", length = 200)
	public String getTxAccessPath() {
		return this.txAccessPath;
	}

	public void setTxAccessPath(String txAccessPath) {
		this.txAccessPath = txAccessPath;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "app")
	public Set<AppProfile> getAppProfiles() {
		return this.appProfiles;
	}

	public void setAppProfiles(Set<AppProfile> appProfiles) {
		this.appProfiles = appProfiles;
	}

}