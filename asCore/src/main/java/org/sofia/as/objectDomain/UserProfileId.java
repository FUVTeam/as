package org.sofia.as.objectDomain;

// default package

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * UserProfileId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class UserProfileId implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -8705980118406369104L;
	private Integer idUser;
	private Integer idProfile;

	// Constructors

	/** default constructor */
	public UserProfileId() {
	}

	/** full constructor */
	public UserProfileId(Integer idUser, Integer idProfile) {
		this.idUser = idUser;
		this.idProfile = idProfile;
	}

	// Property accessors

	@Column(name = "id_user", nullable = false)
	public Integer getIdUser() {
		return this.idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	@Column(name = "id_profile", nullable = false)
	public Integer getIdProfile() {
		return this.idProfile;
	}

	public void setIdProfile(Integer idProfile) {
		this.idProfile = idProfile;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof UserProfileId))
			return false;
		UserProfileId castOther = (UserProfileId) other;

		return ((this.getIdUser() == castOther.getIdUser()) || (this
				.getIdUser() != null && castOther.getIdUser() != null && this
				.getIdUser().equals(castOther.getIdUser())))
				&& ((this.getIdProfile() == castOther.getIdProfile()) || (this
						.getIdProfile() != null
						&& castOther.getIdProfile() != null && this
						.getIdProfile().equals(castOther.getIdProfile())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getIdUser() == null ? 0 : this.getIdUser().hashCode());
		result = 37 * result
				+ (getIdProfile() == null ? 0 : this.getIdProfile().hashCode());
		return result;
	}

}