package org.sofia.as.objectDomain;
// default package

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Branch entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "as010_branch", catalog = "mjas")
public class Branch implements java.io.Serializable {

	// Fields

	private Integer idBranch;
	private String nbBranch;

	// Constructors

	/** default constructor */
	public Branch() {
	}

	/** minimal constructor */
	public Branch(Integer idBranch) {
		this.idBranch = idBranch;
	}

	/** full constructor */
	public Branch(Integer idBranch, String nbBranch) {
		this.idBranch = idBranch;
		this.nbBranch = nbBranch;
	}

	// Property accessors
	@Id
	@Column(name = "id_branch", unique = true, nullable = false)
	public Integer getIdBranch() {
		return this.idBranch;
	}

	public void setIdBranch(Integer idBranch) {
		this.idBranch = idBranch;
	}

	@Column(name = "nb_branch", length = 45)
	public String getNbBranch() {
		return this.nbBranch;
	}

	public void setNbBranch(String nbBranch) {
		this.nbBranch = nbBranch;
	}

}