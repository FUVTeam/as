package org.sofia.as.objectDomain;

// default package

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * AppProfile entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "as007_app_profile", catalog = "mjas")
public class AppProfile implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 756617022052404844L;
	private Integer idApp;
	private Profile profile;
	private App app;
	private Set<AppFunction> appFunctions = new HashSet<AppFunction>(0);

	// Constructors

	/** default constructor */
	public AppProfile() {
	}

	/** minimal constructor */
	public AppProfile(Integer idApp, App app) {
		this.idApp = idApp;
		this.app = app;
	}

	/** full constructor */
	public AppProfile(Integer idApp, Profile profile, App app,
			Set<AppFunction> appFunctions) {
		this.idApp = idApp;
		this.profile = profile;
		this.app = app;
		this.appFunctions = appFunctions;
	}

	// Property accessors
	@Id
	@Column(name = "id_app", unique = true, nullable = false)
	public Integer getIdApp() {
		return this.idApp;
	}

	public void setIdApp(Integer idApp) {
		this.idApp = idApp;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_profile")
	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_app", unique = true, nullable = false, insertable = false, updatable = false)
	public App getApp() {
		return this.app;
	}

	public void setApp(App app) {
		this.app = app;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "appProfile")
	public Set<AppFunction> getAppFunctions() {
		return this.appFunctions;
	}

	public void setAppFunctions(Set<AppFunction> appFunctions) {
		this.appFunctions = appFunctions;
	}

}