package org.sofia.as.objectDomain.vtiger;

import java.util.List;

public class VTigerResult<T> {

	private String success;

	private List<T> result;

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public List<T> getResult() {
		return result;
	}

	public void setResult(List<T> result) {
		this.result = result;
	}

}
