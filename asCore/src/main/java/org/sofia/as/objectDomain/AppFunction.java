package org.sofia.as.objectDomain;

// default package

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * AppFunction entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "as008_app_function", catalog = "mjas")
public class AppFunction implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 7219734521255824601L;
	private Integer idApp;
	private Function function;
	private AppProfile appProfile;
	private String txLevel1;
	private Integer nuLevel;

	// Constructors

	/** default constructor */
	public AppFunction() {
	}

	/** minimal constructor */
	public AppFunction(Integer idApp, AppProfile appProfile) {
		this.idApp = idApp;
		this.appProfile = appProfile;
	}

	/** full constructor */
	public AppFunction(Integer idApp, Function function, AppProfile appProfile,
			String txLevel1, Integer nuLevel) {
		this.idApp = idApp;
		this.function = function;
		this.appProfile = appProfile;
		this.txLevel1 = txLevel1;
		this.nuLevel = nuLevel;
	}

	// Property accessors
	@Id
	@Column(name = "id_app", unique = true, nullable = false)
	public Integer getIdApp() {
		return this.idApp;
	}

	public void setIdApp(Integer idApp) {
		this.idApp = idApp;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_function")
	public Function getFunction() {
		return this.function;
	}

	public void setFunction(Function function) {
		this.function = function;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_app", unique = true, nullable = false, insertable = false, updatable = false)
	public AppProfile getAppProfile() {
		return this.appProfile;
	}

	public void setAppProfile(AppProfile appProfile) {
		this.appProfile = appProfile;
	}

	@Column(name = "tx_level_1", length = 45)
	public String getTxLevel1() {
		return this.txLevel1;
	}

	public void setTxLevel1(String txLevel1) {
		this.txLevel1 = txLevel1;
	}

	@Column(name = "nu_level")
	public Integer getNuLevel() {
		return this.nuLevel;
	}

	public void setNuLevel(Integer nuLevel) {
		this.nuLevel = nuLevel;
	}

}