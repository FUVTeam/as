package org.sofia.as.objectDomain;

// default package

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * ProfileFunctionId entity. @author MyEclipse Persistence Tools
 */
@Embeddable
public class ProfileFunctionId implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 2785828089712937483L;
	private Integer idProfile;
	private Integer idFunction;

	// Constructors

	/** default constructor */
	public ProfileFunctionId() {
	}

	/** full constructor */
	public ProfileFunctionId(Integer idProfile, Integer idFunction) {
		this.idProfile = idProfile;
		this.idFunction = idFunction;
	}

	// Property accessors

	@Column(name = "id_profile", nullable = false)
	public Integer getIdProfile() {
		return this.idProfile;
	}

	public void setIdProfile(Integer idProfile) {
		this.idProfile = idProfile;
	}

	@Column(name = "id_function", nullable = false)
	public Integer getIdFunction() {
		return this.idFunction;
	}

	public void setIdFunction(Integer idFunction) {
		this.idFunction = idFunction;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ProfileFunctionId))
			return false;
		ProfileFunctionId castOther = (ProfileFunctionId) other;

		return ((this.getIdProfile() == castOther.getIdProfile()) || (this
				.getIdProfile() != null && castOther.getIdProfile() != null && this
				.getIdProfile().equals(castOther.getIdProfile())))
				&& ((this.getIdFunction() == castOther.getIdFunction()) || (this
						.getIdFunction() != null
						&& castOther.getIdFunction() != null && this
						.getIdFunction().equals(castOther.getIdFunction())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result
				+ (getIdProfile() == null ? 0 : this.getIdProfile().hashCode());
		result = 37
				* result
				+ (getIdFunction() == null ? 0 : this.getIdFunction()
						.hashCode());
		return result;
	}

}