package org.sofia.as.objectDomain;

// default package

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * UserProfile entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "as003_user_profile", catalog = "mjas")
public class UserProfile implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -1347623955261023222L;
	private UserProfileId id;
	private Profile profile;
	private User user;

	// Constructors

	/** default constructor */
	public UserProfile() {
	}

	/** full constructor */
	public UserProfile(Profile profile, User user) {
		this.profile = profile;
		this.user = user;
	}

	/** full constructor */
	public UserProfile(UserProfileId id, Profile profile, User user) {
		this.id = id;
		this.profile = profile;
		this.user = user;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides({
			@AttributeOverride(name = "idUser", column = @Column(name = "id_user", nullable = false)),
			@AttributeOverride(name = "idProfile", column = @Column(name = "id_profile", nullable = false)) })
	public UserProfileId getId() {
		return this.id;
	}

	public void setId(UserProfileId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_profile", nullable = false, insertable = false, updatable = false)
	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_user", nullable = false, insertable = false, updatable = false)
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}