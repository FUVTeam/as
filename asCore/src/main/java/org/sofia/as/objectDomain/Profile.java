package org.sofia.as.objectDomain;

// default package

import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Profile entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "as002_profile", catalog = "mjas")
public class Profile implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id_profile", unique = true, nullable = false)
	private Integer idProfile;
	@Column(name = "nb_profile", length = 45)
	private String nbProfile;
	@Column(name = "cv_profile", length = 50)
	private String cvProfile;
	@Transient
	private List<Function> functions;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "profile")
	private Set<ProfileFunction> profileFunctions;

	// Constructors

	/** default constructor */
	public Profile() {
		functions = new ArrayList<Function>();
		profileFunctions = new HashSet<ProfileFunction>();
	}

	/** full constructor */
	public Profile(Integer idProfile) {
		this();
		this.idProfile = idProfile;
	}

	/** full constructor */
	public Profile(String nbProfile) {
		this.nbProfile = nbProfile;
	}
	public Integer getIdProfile() {
		return this.idProfile;
	}
	public void setIdProfile(Integer idProfile) {
		this.idProfile = idProfile;
	}
	public String getNbProfile() {
		return this.nbProfile;
	}
	public void setNbProfile(String nbProfile) {
		this.nbProfile = nbProfile;
	}
	public Set<ProfileFunction> getProfileFunctions() {
		return profileFunctions;
	}
	public void setProfileFunctions(Set<ProfileFunction> profileFunctions) {
		this.profileFunctions = profileFunctions;
	}
	public List<Function> getFunctions() {
		return functions;
	}
	public void setFunctions(List<Function> functions) {
		this.functions = functions;
	}
	public String getCvProfile() {
		return cvProfile;
	}
	public void setCvProfile(String cvProfile) {
		this.cvProfile = cvProfile;
	}
	
}