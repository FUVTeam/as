package org.sofia.as.objectDomain;
// default package

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Status entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "as009_status", catalog = "mjas")
public class Status implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -989450896615186562L;
	private Integer idStatus;
	private String cdStatus;
	private String txDescription;

	// Constructors

	/** default constructor */
	public Status() {
	}

	/** full constructor */
	public Status(String cdStatus, String txDescription) {
		this.cdStatus = cdStatus;
		this.txDescription = txDescription;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id_status", unique = true, nullable = false)
	public Integer getIdStatus() {
		return this.idStatus;
	}

	public void setIdStatus(Integer idStatus) {
		this.idStatus = idStatus;
	}

	@Column(name = "cd_status", length = 45)
	public String getCdStatus() {
		return this.cdStatus;
	}

	public void setCdStatus(String cdStatus) {
		this.cdStatus = cdStatus;
	}

	@Column(name = "tx_description", length = 200)
	public String getTxDescription() {
		return this.txDescription;
	}

	public void setTxDescription(String txDescription) {
		this.txDescription = txDescription;
	}

}