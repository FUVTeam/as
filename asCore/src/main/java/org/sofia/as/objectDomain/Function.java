package org.sofia.as.objectDomain;

// default package

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Function entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "as004_function", catalog = "mjas")
public class Function implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 4082234664182212808L;
	private Integer idFunction;
	private String nbFunction;
	private Integer tpFunction;
	private String txAction;
	private String nbIcon;

	// Constructors

	/** default constructor */
	public Function() {
	}

	/** full constructor */
	public Function(String nbFunction, Integer tpFunction, String txAction) {
		this.nbFunction = nbFunction;
		this.tpFunction = tpFunction;
		this.txAction = txAction;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id_function", unique = true, nullable = false)
	public Integer getIdFunction() {
		return this.idFunction;
	}

	public void setIdFunction(Integer idFunction) {
		this.idFunction = idFunction;
	}

	@Column(name = "nb_function", length = 50)
	public String getNbFunction() {
		return this.nbFunction;
	}

	public void setNbFunction(String nbFunction) {
		this.nbFunction = nbFunction;
	}

	@Column(name = "tp_function")
	public Integer getTpFunction() {
		return this.tpFunction;
	}

	public void setTpFunction(Integer tpFunction) {
		this.tpFunction = tpFunction;
	}

	@Column(name = "tx_action", length = 200)
	public String getTxAction() {
		return this.txAction;
	}

	public void setTxAction(String txAction) {
		this.txAction = txAction;
	}

	@Column(name = "nb_icon", length = 50)
	public String getNbIcon() {
		return nbIcon;
	}

	public void setNbIcon(String nbIcon) {
		this.nbIcon = nbIcon;
	}

}