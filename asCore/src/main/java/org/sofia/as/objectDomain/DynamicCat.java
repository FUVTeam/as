package org.sofia.as.objectDomain;

public class DynamicCat {

	private String object;
	private String selectObject;
	private String idProperty;
	private String labelProperty;

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}

	public String getSelectObject() {
		return selectObject;
	}

	public void setSelectObject(String selectObject) {
		this.selectObject = selectObject;
	}

	public String getIdProperty() {
		return idProperty;
	}

	public void setIdProperty(String idProperty) {
		this.idProperty = idProperty;
	}

	public String getLabelProperty() {
		return labelProperty;
	}

	public void setLabelProperty(String labelProperty) {
		this.labelProperty = labelProperty;
	}

}
