package org.sofia.as.objectDomain.vtiger;

import org.sofia.as.objectDomain.User;

public class VTigerUser {

	private String user_name;
	private String is_admin = "off";
	private String user_password;
	private String confirm_password;
	private String first_name;
	private String last_name;
	private String roleid = "H5";
	private String email1;
	private String status = "Active";
	private String activity_view = "This Week";
	private String lead_view = "Today";
	private String hour_format = "12";
	private String end_hour = "23:00";
	private String start_hour = "00:00";
	private String title = "";
	private String phone_work = "";
	private String department = "";
	private String phone_mobile = "";
	private String reports_to_id = "";
	private String phone_other = "";
	private String email2 = "";
	private String phone_fax = "";
	private String secondaryemail = "";
	private String phone_home = "";
	private String date_format = "dd-mm-yyyy";
	private String signature = "";
	private String description = "";
	private String address_street = "";
	private String address_city = "";
	private String address_state = "";
	private String address_postalcode = "";
	private String address_country = "";
	private String accesskey = "";
	private String time_zone = "America/Mexico_City";
	private String currency_id = "21x1";
	private String currency_grouping_pattern = "123,456,789";
	private String currency_decimal_separator = ".";
	private String currency_grouping_separator = ",";
	private String currency_symbol_placement = "$1.0";
	private String imagename = "";
	private String internal_mailer = "1";
	private String theme = "alphagrey";
	private String language = "es_mx";
	private String reminder_interval = "1 Minute";
	private String phone_crm_extension = "";
	private String no_of_currency_decimals = "2";
	private String truncate_trailing_zeros = "1";
	private String dayoftheweek = "Sunday";
	private String callduration = "5";
	private String othereventduration = "5";
	private String calendarsharedtype = "public";
	private String default_record_view = "Summary";
	private String leftpanelhide = "0";
	private String rowheight = "medium";
	private String defaulteventstatus = "";
	private String defaultactivitytype = "";
	private String hidecompletedevents = "0";
	private String is_owner = "1";
	private String id;

	public VTigerUser() {
	}

	public VTigerUser(User user) {
		this.user_name = user.getNbEmail();
		this.user_password = user.getPwPrincipal();
		this.confirm_password = user.getPwPrincipal();
		this.first_name = user.getNbFirst();
		this.last_name = user.getNbLast();
		this.email1 = user.getNbEmail();
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getIs_admin() {
		return is_admin;
	}

	public void setIs_admin(String is_admin) {
		this.is_admin = is_admin;
	}

	public String getUser_password() {
		return user_password;
	}

	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}

	public String getConfirm_password() {
		return confirm_password;
	}

	public void setConfirm_password(String confirm_password) {
		this.confirm_password = confirm_password;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getRoleid() {
		return roleid;
	}

	public void setRoleid(String roleid) {
		this.roleid = roleid;
	}

	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getActivity_view() {
		return activity_view;
	}

	public void setActivity_view(String activity_view) {
		this.activity_view = activity_view;
	}

	public String getLead_view() {
		return lead_view;
	}

	public void setLead_view(String lead_view) {
		this.lead_view = lead_view;
	}

	public String getHour_format() {
		return hour_format;
	}

	public void setHour_format(String hour_format) {
		this.hour_format = hour_format;
	}

	public String getEnd_hour() {
		return end_hour;
	}

	public void setEnd_hour(String end_hour) {
		this.end_hour = end_hour;
	}

	public String getStart_hour() {
		return start_hour;
	}

	public void setStart_hour(String start_hour) {
		this.start_hour = start_hour;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPhone_work() {
		return phone_work;
	}

	public void setPhone_work(String phone_work) {
		this.phone_work = phone_work;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getPhone_mobile() {
		return phone_mobile;
	}

	public void setPhone_mobile(String phone_mobile) {
		this.phone_mobile = phone_mobile;
	}

	public String getReports_to_id() {
		return reports_to_id;
	}

	public void setReports_to_id(String reports_to_id) {
		this.reports_to_id = reports_to_id;
	}

	public String getPhone_other() {
		return phone_other;
	}

	public void setPhone_other(String phone_other) {
		this.phone_other = phone_other;
	}

	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public String getPhone_fax() {
		return phone_fax;
	}

	public void setPhone_fax(String phone_fax) {
		this.phone_fax = phone_fax;
	}

	public String getSecondaryemail() {
		return secondaryemail;
	}

	public void setSecondaryemail(String secondaryemail) {
		this.secondaryemail = secondaryemail;
	}

	public String getPhone_home() {
		return phone_home;
	}

	public void setPhone_home(String phone_home) {
		this.phone_home = phone_home;
	}

	public String getDate_format() {
		return date_format;
	}

	public void setDate_format(String date_format) {
		this.date_format = date_format;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAddress_street() {
		return address_street;
	}

	public void setAddress_street(String address_street) {
		this.address_street = address_street;
	}

	public String getAddress_city() {
		return address_city;
	}

	public void setAddress_city(String address_city) {
		this.address_city = address_city;
	}

	public String getAddress_state() {
		return address_state;
	}

	public void setAddress_state(String address_state) {
		this.address_state = address_state;
	}

	public String getAddress_postalcode() {
		return address_postalcode;
	}

	public void setAddress_postalcode(String address_postalcode) {
		this.address_postalcode = address_postalcode;
	}

	public String getAddress_country() {
		return address_country;
	}

	public void setAddress_country(String address_country) {
		this.address_country = address_country;
	}

	public String getAccesskey() {
		return accesskey;
	}

	public void setAccesskey(String accesskey) {
		this.accesskey = accesskey;
	}

	public String getTime_zone() {
		return time_zone;
	}

	public void setTime_zone(String time_zone) {
		this.time_zone = time_zone;
	}

	public String getCurrency_id() {
		return currency_id;
	}

	public void setCurrency_id(String currency_id) {
		this.currency_id = currency_id;
	}

	public String getCurrency_grouping_pattern() {
		return currency_grouping_pattern;
	}

	public void setCurrency_grouping_pattern(String currency_grouping_pattern) {
		this.currency_grouping_pattern = currency_grouping_pattern;
	}

	public String getCurrency_decimal_separator() {
		return currency_decimal_separator;
	}

	public void setCurrency_decimal_separator(String currency_decimal_separator) {
		this.currency_decimal_separator = currency_decimal_separator;
	}

	public String getCurrency_grouping_separator() {
		return currency_grouping_separator;
	}

	public void setCurrency_grouping_separator(
			String currency_grouping_separator) {
		this.currency_grouping_separator = currency_grouping_separator;
	}

	public String getCurrency_symbol_placement() {
		return currency_symbol_placement;
	}

	public void setCurrency_symbol_placement(String currency_symbol_placement) {
		this.currency_symbol_placement = currency_symbol_placement;
	}

	public String getImagename() {
		return imagename;
	}

	public void setImagename(String imagename) {
		this.imagename = imagename;
	}

	public String getInternal_mailer() {
		return internal_mailer;
	}

	public void setInternal_mailer(String internal_mailer) {
		this.internal_mailer = internal_mailer;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getReminder_interval() {
		return reminder_interval;
	}

	public void setReminder_interval(String reminder_interval) {
		this.reminder_interval = reminder_interval;
	}

	public String getPhone_crm_extension() {
		return phone_crm_extension;
	}

	public void setPhone_crm_extension(String phone_crm_extension) {
		this.phone_crm_extension = phone_crm_extension;
	}

	public String getNo_of_currency_decimals() {
		return no_of_currency_decimals;
	}

	public void setNo_of_currency_decimals(String no_of_currency_decimals) {
		this.no_of_currency_decimals = no_of_currency_decimals;
	}

	public String getTruncate_trailing_zeros() {
		return truncate_trailing_zeros;
	}

	public void setTruncate_trailing_zeros(String truncate_trailing_zeros) {
		this.truncate_trailing_zeros = truncate_trailing_zeros;
	}

	public String getDayoftheweek() {
		return dayoftheweek;
	}

	public void setDayoftheweek(String dayoftheweek) {
		this.dayoftheweek = dayoftheweek;
	}

	public String getCallduration() {
		return callduration;
	}

	public void setCallduration(String callduration) {
		this.callduration = callduration;
	}

	public String getOthereventduration() {
		return othereventduration;
	}

	public void setOthereventduration(String othereventduration) {
		this.othereventduration = othereventduration;
	}

	public String getCalendarsharedtype() {
		return calendarsharedtype;
	}

	public void setCalendarsharedtype(String calendarsharedtype) {
		this.calendarsharedtype = calendarsharedtype;
	}

	public String getDefault_record_view() {
		return default_record_view;
	}

	public void setDefault_record_view(String default_record_view) {
		this.default_record_view = default_record_view;
	}

	public String getLeftpanelhide() {
		return leftpanelhide;
	}

	public void setLeftpanelhide(String leftpanelhide) {
		this.leftpanelhide = leftpanelhide;
	}

	public String getRowheight() {
		return rowheight;
	}

	public void setRowheight(String rowheight) {
		this.rowheight = rowheight;
	}

	public String getDefaulteventstatus() {
		return defaulteventstatus;
	}

	public void setDefaulteventstatus(String defaulteventstatus) {
		this.defaulteventstatus = defaulteventstatus;
	}

	public String getDefaultactivitytype() {
		return defaultactivitytype;
	}

	public void setDefaultactivitytype(String defaultactivitytype) {
		this.defaultactivitytype = defaultactivitytype;
	}

	public String getHidecompletedevents() {
		return hidecompletedevents;
	}

	public void setHidecompletedevents(String hidecompletedevents) {
		this.hidecompletedevents = hidecompletedevents;
	}

	public String getIs_owner() {
		return is_owner;
	}

	public void setIs_owner(String is_owner) {
		this.is_owner = is_owner;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
