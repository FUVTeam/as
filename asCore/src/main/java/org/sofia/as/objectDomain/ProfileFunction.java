package org.sofia.as.objectDomain;

// default package

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * ProfileFunction entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "as005_profile_function", catalog = "mjas")
public class ProfileFunction implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 7826138003295327399L;
	private ProfileFunctionId id;
	private Profile profile;
	private Function function;

	// Constructors

	/** default constructor */
	public ProfileFunction() {
	}

	/** full constructor */
	public ProfileFunction(ProfileFunctionId id, Profile profile,
			Function function) {
		this.id = id;
		this.profile = profile;
		this.function = function;
	}

	// Property accessors
	@EmbeddedId
	@AttributeOverrides({
			@AttributeOverride(name = "idProfile", column = @Column(name = "id_profile", nullable = false)),
			@AttributeOverride(name = "idFunction", column = @Column(name = "id_function", nullable = false)) })
	public ProfileFunctionId getId() {
		return this.id;
	}

	public void setId(ProfileFunctionId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_profile", nullable = false, insertable = false, updatable = false)
	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_function", nullable = false, insertable = false, updatable = false)
	public Function getFunction() {
		return this.function;
	}

	public void setFunction(Function function) {
		this.function = function;
	}

}