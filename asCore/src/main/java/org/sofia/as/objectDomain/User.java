package org.sofia.as.objectDomain;

// default package

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * User entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "as001_user", catalog = "mjas")
public class User implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -4616368108143270672L;
	private Integer idUser;
	private String nbFirst;
	private String cdFirstAccess;
	private String idVtiger;
	private String nbLast;
	private String nbMothers;
	private Date fhBirth;
	private Integer cdStatus;
	private Integer idBranch;
	private String nbBranch;
	private String nbEmail;
	private String pwTemp;
	private String pwPrincipal;
	private Integer nuFailedAttempts;
	private Timestamp tmFirstLogin;
	private Integer cdActiveSession;
	private Integer idProfile;
	private List<Function> functions;

	// Constructors

	/** default constructor */
	public User() {
	}

	/** full constructor */
	public User(Integer idUser, String nbFirst, String nbLast,
			String nbMothers, Date fhBirth, Integer cdStatus, String nbEmail,
			String pwTemp, String pwPrincipal, Integer nuFailedAttempts,
			Date tmFirstLogin, Integer cdActiveSession, Integer idProfile) {
		this.idUser = idUser;
		this.nbFirst = nbFirst;
		this.nbLast = nbLast;
		this.nbMothers = nbMothers;
		this.fhBirth = fhBirth;
		this.cdStatus = cdStatus;
		this.nbEmail = nbEmail;
		this.pwTemp = pwTemp;
		this.pwPrincipal = pwPrincipal;
		this.nuFailedAttempts = nuFailedAttempts;
		this.tmFirstLogin = new Timestamp(tmFirstLogin.getTime());
		this.cdActiveSession = cdActiveSession;
		this.idProfile = idProfile;
	}

	public User(String nbFirst, String nbLast, String nbMothers, Date fhBirth,
			Integer cdStatus, String nbEmail, Integer idBranch,
			String nbBranch, String idVtiger) {
		this.nbFirst = nbFirst;
		this.nbLast = nbLast;
		this.nbMothers = nbMothers;
		this.fhBirth = fhBirth;
		this.cdStatus = cdStatus;
		this.nbEmail = nbEmail;
		this.idBranch = idBranch;
		this.nbBranch = nbBranch;
		this.idVtiger = idVtiger;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id_user", unique = true, nullable = false)
	public Integer getIdUser() {
		return this.idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	@Column(name = "nb_first", length = 50)
	public String getNbFirst() {
		return this.nbFirst;
	}

	public void setNbFirst(String nbFirst) {
		this.nbFirst = nbFirst;
	}

	@Column(name = "nb_last", length = 45)
	public String getNbLast() {
		return this.nbLast;
	}

	public void setNbLast(String nbLast) {
		this.nbLast = nbLast;
	}

	@Column(name = "nb_mothers", length = 45)
	public String getNbMothers() {
		return this.nbMothers;
	}

	public void setNbMothers(String nbMothers) {
		this.nbMothers = nbMothers;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "fh_birth", length = 10)
	public Date getFhBirth() {
		return this.fhBirth;
	}

	public void setFhBirth(Date fhBirth) {
		this.fhBirth = fhBirth;
	}

	@Column(name = "cd_status")
	public Integer getCdStatus() {
		return this.cdStatus;
	}

	public void setCdStatus(Integer cdStatus) {
		this.cdStatus = cdStatus;
	}

	@Column(name = "nb_email", length = 45)
	public String getNbEmail() {
		return this.nbEmail;
	}

	public void setNbEmail(String nbEmail) {
		this.nbEmail = nbEmail;
	}

	@Column(name = "pw_temp", length = 45)
	public String getPwTemp() {
		return this.pwTemp;
	}

	public void setPwTemp(String pwTemp) {
		this.pwTemp = pwTemp;
	}

	@Column(name = "pw_principal", length = 45)
	public String getPwPrincipal() {
		return this.pwPrincipal;
	}

	public void setPwPrincipal(String pwPrincipal) {
		this.pwPrincipal = pwPrincipal;
	}

	@Column(name = "nu_failed_attempts")
	public Integer getNuFailedAttempts() {
		return this.nuFailedAttempts;
	}

	public void setNuFailedAttempts(Integer nuFailedAttempts) {
		this.nuFailedAttempts = nuFailedAttempts;
	}

	@Column(name = "tm_first_login", length = 10)
	public Timestamp getTmFirstLogin() {
		return this.tmFirstLogin;
	}

	public void setTmFirstLogin(Timestamp tmFirstLogin) {
		this.tmFirstLogin = tmFirstLogin;
	}

	@Column(name = "cd_active_session")
	public Integer getCdActiveSession() {
		return this.cdActiveSession;
	}

	public void setCdActiveSession(Integer cdActiveSession) {
		this.cdActiveSession = cdActiveSession;
	}

	@Transient
	public Integer getIdProfile() {
		return idProfile;
	}

	public void setIdProfile(Integer idProfile) {
		this.idProfile = idProfile;
	}

	@Column(name = "id_vtiger", length = 45)
	public String getIdVtiger() {
		return idVtiger;
	}

	public void setIdVtiger(String idVtiger) {
		this.idVtiger = idVtiger;
	}

	@Column(name = "cd_first_access", length = 200)
	public String getCdFirstAccess() {
		return cdFirstAccess;
	}

	public void setCdFirstAccess(String cdFirstAccess) {
		this.cdFirstAccess = cdFirstAccess;
	}

	@Column(name = "id_branch")
	public Integer getIdBranch() {
		return idBranch;
	}

	public void setIdBranch(Integer idBranch) {
		this.idBranch = idBranch;
	}

	@Column(name = "nb_branch", length = 200)
	public String getNbBranch() {
		return nbBranch;
	}

	public void setNbBranch(String nbBranch) {
		this.nbBranch = nbBranch;
	}

	@Transient
	public List<Function> getFunctions() {
		return functions;
	}

	public void setFunctions(List<Function> functions) {
		this.functions = functions;
	}

}