package org.sofia.as.jpa.repositories;

import org.sofia.as.objectDomain.UserProfile;
import org.sofia.as.objectDomain.UserProfileId;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Siva
 * 
 */
public interface UserProfileRepository extends
		JpaRepository<UserProfile, UserProfileId> {

}
