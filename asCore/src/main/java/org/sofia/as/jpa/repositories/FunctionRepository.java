package org.sofia.as.jpa.repositories;

import java.util.List;

import org.sofia.as.objectDomain.Function;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Siva
 * 
 */
@Transactional
public interface FunctionRepository extends JpaRepository<Function, Integer> {

	@Query("select a from Function a, ProfileFunction b, UserProfile c, User d where "
			+ "a.idFunction = b.id.idFunction and b.id.idProfile = c.id.idProfile and c.id.idUser = d.idUser and d.idUser = ?1")
	public List<Function> findByIdUser(Integer iduser);

}
