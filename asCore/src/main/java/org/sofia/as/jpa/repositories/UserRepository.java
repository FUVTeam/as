package org.sofia.as.jpa.repositories;

import java.util.Date;
import java.util.List;

import org.sofia.as.objectDomain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Siva
 * 
 */
@Transactional
public interface UserRepository extends JpaRepository<User, Integer> {

	@Query("SELECT new User(a.idUser, a.nbFirst, a.nbLast, a.nbMothers, a.fhBirth, a.cdStatus, a.nbEmail, a.pwTemp, a.pwPrincipal, a.nuFailedAttempts, a.tmFirstLogin, "
			+ "a.cdActiveSession, b.profile.idProfile) FROM User a, UserProfile b, AppProfile c where a.idUser = b.user.idUser and b.profile.idProfile = c.profile.idProfile and c.idApp = ?1")
	public List<User> findByIdApp(Integer idApp);

	@Query("SELECT new User(a.nbFirst, a.nbLast, a.nbMothers, a.fhBirth, a.cdStatus, a.nbEmail, a.idBranch, a.nbBranch, a.idVtiger) FROM User a")
	public List<User> findAdvisors();

	public List<User> findByNbEmail(String nbEmail);

	public List<User> findByNbEmailAndCdStatus(String nbEmail, Integer cdStatus);

	public User findByCdFirstAccessAndIdUser(String cdFirstAccess,
			Integer idUser);

	public User findByPwTempAndNbEmail(String pwTemp, String nbEmail);

	public User findByPwPrincipalAndNbEmail(String pwPrincipal, String nbEmail);

	@Modifying
	@Query("update User u set u.cdFirstAccess = null where u.idUser = ?1")
	public int updateFirstAccess(Integer idUser);

	@Modifying
	@Query("update User u set stUser = 1, u.pwPrincipal = ?1, u.pwTemp = NULL, u.cdFirstAccess = NULL where u.idUser = ?2")
	public int updatePassword(String pwPrincipal, Integer idUser);

	@Modifying
	@Query("update User u set u.cdStatus = ?1 where u.idUser = ?2")
	public int updateStatus(Integer idStatus, Integer idUser);

	@Modifying
	@Query("update User u set u.cdStatus = ?1, u.nbFirst = ?2, u.nbLast = ?3, u.nbMothers = ?4, u.nbEmail = ?5, "
			+ " u.fhBirth = ?6, u.idBranch = ?7, u.nbBranch = ?8 where u.idUser = ?9")
	public int updateUser(Integer idStatus, String nbForst, String nbLast,
			String nbMothers, String nbEmail, Date fhBirth, Integer idBranch,
			String nbBranch, Integer idUser);

}
