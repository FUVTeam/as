package org.sofia.as.utils;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;

import javax.mail.internet.MimeMessage;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class MessageServiceImpl implements IMessageService {

	public void sendEmailMessage(HashMap<String, Object> mailParams)
			throws Exception {
		try {
			String idMessage = (String) mailParams.get("idMessage");
			Resource resource = new ClassPathResource(
					"org/sofia/as/asWeb-application.properties");
			Properties properties = PropertiesLoaderUtils
					.loadProperties(resource);

			JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
			javaMailSender.setDefaultEncoding("ISO-8859-1");
			javaMailSender.setHost(properties.getProperty("host.smtp"));
			javaMailSender.setPort(Integer.parseInt(properties
					.getProperty("port.smtp")));
			javaMailSender.setProtocol("smtp");
			javaMailSender.setUsername(properties.getProperty("user.smtp"));
			javaMailSender.setPassword(properties.getProperty("password.smtp"));

			Properties props = getMailProperties(properties
					.getProperty("properties.smtp"));

			javaMailSender.setJavaMailProperties(props);
			MimeMessage message = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, true);

			helper.setFrom(properties
					.getProperty("from." + idMessage + ".smtp"));
			helper.setTo(((String) mailParams.get("destiny")).split(","));

			helper.setSubject(properties.getProperty("subject." + idMessage
					+ ".smtp"));

			String html = getHtml(getTxtTemplate(idMessage), mailParams);

			helper.setText(html, true);
			javaMailSender.send(message);
		} catch (Throwable e) {
			throw new Exception(e.getMessage());
		}
	}

	private Properties getMailProperties(String txProperties) {
		Properties properties = new Properties();
		String[] array = txProperties.split(" ");
		for (String p : array) {
			properties.setProperty(p.split("=")[0].trim(),
					p.split("=")[1].trim());
		}
		return properties;
	}

	private String getHtml(String txTemplate, HashMap<String, Object> values) {
		String html = txTemplate;
		Set<String> keySet = values.keySet();
		for (String key : keySet) {
			try {
				html = html.replace("${" + key + "}", (String) values.get(key));
			} catch (Exception localException) {
			}
		}
		return html;
	}

	private String getTxtTemplate(String idTemplate) {
		String txTemplate = "";
		try {
			String nbFile = "/app/vu/mail/";

			File file = new File(nbFile + idTemplate + ".txt");

			FileInputStream fstream = new FileInputStream(file);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader buffer = new BufferedReader(
					new InputStreamReader(in));
			String str;
			while ((str = buffer.readLine()) != null) {
				txTemplate = txTemplate + str;
			}
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return txTemplate;
	}

	public static void main(String[] args) {

		HashMap<String, Object> mailParams = new HashMap<String, Object>();
		mailParams.put("idMessage", "1");
		mailParams.put("destiny", "fjbleal@gmail.com");

		MessageServiceImpl messageServiceImpl = new MessageServiceImpl();
		try {
			messageServiceImpl.sendEmailMessage(mailParams);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
