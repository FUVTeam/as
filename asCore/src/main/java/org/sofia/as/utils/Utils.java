package org.sofia.as.utils;

import java.io.IOException;
import java.security.MessageDigest;
import java.util.HashMap;

import org.apache.commons.lang.RandomStringUtils;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Utils {

	public static String getPropertyFromJsonString(String jsonString,
			String property) throws IOException {
		JsonFactory factory = new JsonFactory();
		ObjectMapper mapper = new ObjectMapper(factory);
		TypeReference<HashMap<String, Object>> typeRef = new TypeReference<HashMap<String, Object>>() {
		};
		HashMap<String, Object> o = mapper.readValue(jsonString, typeRef);
		return o.get(property).toString();
	}

	public static String generateRandomPassword() {
		return RandomStringUtils.random(8, true, true);
	}

	public static String generateRandomCode() {
		return RandomStringUtils.random(16, true, false);
	}

	public static String MD5Hash(String str) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(str.getBytes());
			byte byteData[] = md.digest();
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16)
						.substring(1));
			}
			return sb.toString();
		} catch (Exception e) {
			return null;
		}
	}

}
