package org.sofia.as.utils;

import java.util.HashMap;

public interface IMessageService {

	public void sendEmailMessage(HashMap<String, Object> mailParams)
			throws Exception;
}
