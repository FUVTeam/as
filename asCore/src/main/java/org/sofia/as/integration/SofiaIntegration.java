package org.sofia.as.integration;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Service
public class SofiaIntegration {

	public String executeSofiaRestCommand(String alias, Object request) {
		RestTemplate restTemplate = new RestTemplate();
		String result = null;
		try {
			String sofiaURL = "http://localhost:7080/ssbWsEp/rest/";
			// String sofiaURL = "http://localhost:7080/ssbWsEp/rest/";
			result = restTemplate.postForObject(new URI(sofiaURL + alias
					+ "/json"), request, String.class);
		} catch (RestClientException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return result;
	}

}
