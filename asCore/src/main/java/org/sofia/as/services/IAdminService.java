package org.sofia.as.services;

import java.util.List;

public interface IAdminService<T> {

	public List<T> findAll() throws Exception;

	public T save(T object) throws Exception;

	public T update(T object) throws Exception;

	public void delete(T object) throws Exception;

}
