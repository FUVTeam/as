package org.sofia.as.services.admin;

import java.util.List;

import org.sofia.as.objectDomain.DynamicCat;
import org.sofia.as.services.IAdminService;

public interface IGenericService extends IAdminService<Object> {

	public List<Object> findAll(String objectType) throws Exception;

	public List<Object> findAllComnboValues(DynamicCat dynamicCat)
			throws Exception;

}
