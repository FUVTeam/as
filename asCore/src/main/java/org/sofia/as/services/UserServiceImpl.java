package org.sofia.as.services;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.sofia.as.integration.SofiaIntegration;
import org.sofia.as.jpa.repositories.FunctionRepository;
import org.sofia.as.jpa.repositories.UserProfileRepository;
import org.sofia.as.jpa.repositories.UserRepository;
import org.sofia.as.objectDomain.Function;
import org.sofia.as.objectDomain.Profile;
import org.sofia.as.objectDomain.User;
import org.sofia.as.objectDomain.UserProfile;
import org.sofia.as.objectDomain.UserProfileId;
import org.sofia.as.objectDomain.vtiger.VTigerRequest;
import org.sofia.as.objectDomain.vtiger.VTigerUser;
import org.sofia.as.utils.IMessageService;
import org.sofia.as.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class UserServiceImpl implements IUserService {

	private final String SESSION_INVALID = "INVALID_SESSIONID";
	private String vTigerSessionName = null;

	@Autowired
	private SofiaIntegration sofiaIntegration;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private FunctionRepository functionRepository;

	@Autowired
	private UserProfileRepository userProfileRepository;

	@Autowired
	private IMessageService messageService;

	@Override
	public List<User> getUsers(Integer idApp) throws Exception {
		try {
			List<User> list = userRepository.findByIdApp(idApp);
			return list;
		} catch (Exception e) {
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public User addUser(User user) throws Exception {
		try {

			List<User> users = userRepository.findByNbEmail(user.getNbEmail());
			if (users != null && !users.isEmpty()) {
				if (userRepository.findByNbEmail(user.getNbEmail()) != null) {
					throw new Exception(
							"_%Ya existe un usuario con el correo proporcionado.%_");
				}
			}

			String pwPassword = Utils.generateRandomPassword();
			user.setPwTemp(Utils.MD5Hash(pwPassword));
			user.setPwPrincipal(pwPassword);

			if (vTigerSessionName == null) {
				vTigerSessionName = login();
			}

			VTigerRequest vTigerRequest = new VTigerRequest();
			vTigerRequest.setSessionName(vTigerSessionName);
			vTigerRequest.setOperation("create");
			vTigerRequest.setElement(new VTigerUser(user));
			vTigerRequest.setElementType("Users");
			String resultObject = sofiaIntegration.executeSofiaRestCommand(
					"IND00004AC", vTigerRequest);

			String errorMessage = verifyResult(resultObject);
			if (errorMessage != null) {
				if (errorMessage.contains(SESSION_INVALID)) {
					vTigerSessionName = login();
					resultObject = sofiaIntegration.executeSofiaRestCommand(
							"IND00004AC", vTigerRequest);
				}
				throw new Exception(
						"_%No es posible agregar el usuario en vTiger%_"
								+ errorMessage);
			}

			JsonFactory factory = new JsonFactory();
			ObjectMapper mapper = new ObjectMapper(factory);
			TypeReference<HashMap<String, Object>> typeRef = new TypeReference<HashMap<String, Object>>() {
			};
			HashMap<String, Object> o = mapper.readValue(resultObject, typeRef);
			String idVtigerUser = ((Map<String, Object>) o.get("result")).get(
					"id").toString();
			user.setIdVtiger(idVtigerUser);

			String code = Utils.generateRandomCode();
			code = Utils.MD5Hash(code);
			user.setCdFirstAccess(code);
			user.setCdStatus(2);
			User result = userRepository.save(user);
			userProfileRepository.save(new UserProfile(new UserProfileId(result
					.getIdUser(), user.getIdProfile()), new Profile(user
					.getIdProfile()), result));

			code = code.length() + "VU" + code + result.getIdUser();
			HashMap<String, Object> mailParams = new HashMap<String, Object>();
			mailParams.put("idMessage", "1");
			mailParams.put("destiny", user.getNbEmail());
			mailParams.put("pwTmpPassword", pwPassword);
			mailParams.put("code", code);
			messageService.sendEmailMessage(mailParams);

			return result;
		} catch (Exception e) {
			throw e;
		}
	}

	private String login() throws Exception {

		VTigerRequest vTigerRequest = new VTigerRequest();
		vTigerRequest.setUserName("admin");
		vTigerRequest.setAccessKey("mBr89prVVwVLWzXf");

		String result = sofiaIntegration.executeSofiaRestCommand("IND00004AA",
				vTigerRequest);
		String sessionName = Utils.getPropertyFromJsonString(result,
				"sessionName");
		return sessionName;
	}

	@SuppressWarnings("unchecked")
	private String verifyResult(String resultString) {
		try {
			JsonFactory factory = new JsonFactory();
			ObjectMapper mapper = new ObjectMapper(factory);
			TypeReference<HashMap<String, Object>> typeRef = new TypeReference<HashMap<String, Object>>() {
			};
			HashMap<String, Object> o = mapper.readValue(resultString, typeRef);
			if (!(Boolean) o.get("success")) {
				Map<String, Object> result = (Map<String, Object>) o
						.get("error");
				String message = result.get("code").toString() + " - "
						+ result.get("message");
				return message;
			} else {
				return null;
			}
		} catch (Exception e) {
			return e.getMessage();
		}

	}

	@Override
	public void verifyCode(String code) throws Exception {
		Integer length = Integer.valueOf(code.split("VU")[0]);
		Integer id = Integer.valueOf(code.substring(length + 2
				+ String.valueOf(length).length()));
		String exCode = code.split("VU")[1].substring(0,
				code.split("VU")[1].length() - String.valueOf(id).length());
		if (userRepository.findOne(id) == null
				|| userRepository.findByCdFirstAccessAndIdUser(exCode, id) == null) {
			throw new Exception("Ivalid Code");
		}

		userRepository.updateFirstAccess(id);

	}

	@Override
	public void activate(String pwPrincipal, String pwTemp, String nbEmail)
			throws Exception {
		try {
			User user = userRepository.findByPwTempAndNbEmail(pwTemp, nbEmail);
			if (user == null) {
				throw new Exception("Ivalid Credentials");
			}

			userRepository.updatePassword(pwPrincipal, user.getIdUser());
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Ivalid Code");
		}
	}

	@Override
	public User validate(String pwPrincipal, String nbEmail) throws Exception {
		try {
			User user = null;
			List<User> users = userRepository.findByNbEmailAndCdStatus(nbEmail,
					1);
			if (users == null) {
				throw new Exception("Ivalid Credentials");
			} else {

				user = users.get(0);
				if (user.getPwPrincipal().equals(pwPrincipal)) {
					user.setCdActiveSession(1);
					user.setNuFailedAttempts(0);
					user.setTmFirstLogin(new Timestamp(System
							.currentTimeMillis()));
					updateUser(user, false);

					List<Function> functions = functionRepository
							.findByIdUser(user.getIdUser());
					user.setFunctions(functions);
				} else {
					Integer i = user.getNuFailedAttempts();
					if (i == null) {
						i = 1;
					} else {
						i++;
					}
					user.setNuFailedAttempts(i);
					updateUser(user, false);
					throw new Exception("Ivalid Credentials");
				}

			}
			return user;

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Ivalid Data");
		}
	}

	@Override
	public User recoverUser(String nbEmail) throws Exception {
		try {
			List<User> users = userRepository.findByNbEmail(nbEmail);
			if (users != null && users.size() > 0) {
				return new User();
			}

			String pwPassword = Utils.generateRandomPassword();
			User user = users.get(0);
			user.setPwTemp(Utils.MD5Hash(pwPassword));
			user.setPwPrincipal(pwPassword);
			user.setCdStatus(3);

			String code = Utils.generateRandomCode();
			code = Utils.MD5Hash(code);
			user.setCdFirstAccess(code);
			User result = userRepository.save(user);

			code = code.length() + "VU" + code + result.getIdUser();
			HashMap<String, Object> mailParams = new HashMap<String, Object>();
			mailParams.put("idMessage", "2");
			mailParams.put("destiny", user.getNbEmail());
			mailParams.put("pwTmpPassword", pwPassword);
			mailParams.put("code", code);
			messageService.sendEmailMessage(mailParams);
			return new User();
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<User> getAdvisors() throws Exception {
		try {
			return userRepository.findAdvisors();
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public void updateUser(User user, boolean updateProfile) throws Exception {
		try {

			List<User> users = userRepository.findByNbEmail(user.getNbEmail());
			if (users != null && users.size() > 1) {
				throw new Exception(
						"_%Ya existe un usuario con el correo proporcionado.%_");
			}

			userRepository.updateUser(user.getCdStatus(), user.getNbFirst(),
					user.getNbLast(), user.getNbMothers(), user.getNbEmail(),
					user.getFhBirth(), user.getIdBranch(), user.getNbBranch(),
					user.getIdUser());

			if (updateProfile) {
				userProfileRepository.save(new UserProfile(new UserProfileId(
						user.getIdUser(), user.getIdProfile()), new Profile(
						user.getIdProfile()), user));
			}

		} catch (Exception e) {
			throw e;
		}
	}

}
