package org.sofia.as.services;

import java.util.List;

import org.sofia.as.objectDomain.User;

public interface IUserService {

	public List<User> getUsers(Integer idApp) throws Exception;

	public User addUser(User user) throws Exception;

	public void verifyCode(String code) throws Exception;

	public void activate(String pwPrincipal, String pwTemp, String nbEmail)
			throws Exception;

	public User validate(String pwPrincipal, String nbEmail) throws Exception;

	public User recoverUser(String nbEmail) throws Exception;

	public List<User> getAdvisors() throws Exception;

	public void updateUser(User user, boolean updateProfile) throws Exception;

}
