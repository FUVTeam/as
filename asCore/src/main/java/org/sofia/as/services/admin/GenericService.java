package org.sofia.as.services.admin;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.sofia.as.objectDomain.DynamicCat;
import org.sofia.as.objectDomain.Profile;
import org.sofia.as.objectDomain.ProfileFunction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class GenericService implements IGenericService {

	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<Object> findAll(String objectType) throws Exception {
		try {
			Query query = entityManager.createQuery("FROM " + objectType);
			return query.getResultList();
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Object save(Object object) throws Exception {
		try {

			if (object instanceof Profile) {
				Profile p = (Profile) object;
				Set<ProfileFunction> functions = p.getProfileFunctions();
				p.setProfileFunctions(new HashSet<ProfileFunction>(0));

				p = entityManager.merge(p);

				for (ProfileFunction profileFunction : functions) {
					profileFunction.getId().setIdProfile(p.getIdProfile());
				}

				p.setProfileFunctions(functions);
				entityManager.merge(p);

			} else {
				entityManager.merge(object);
			}
			return object;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Object update(Object object) throws Exception {
		try {
			// return airlineRepository.save(object);
			return null;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public void delete(Object object) throws Exception {
		try {
			// airlineRepository.delete(object);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<Object> findAll() throws Exception {
		return null;
	}

	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object> findAllComnboValues(DynamicCat dynamicCat)
			throws Exception {
		try {
			String query = "SELECT " + dynamicCat.getIdProperty() + ","
					+ dynamicCat.getLabelProperty() + " FROM "
					+ dynamicCat.getSelectObject();
			List<Object> objects = entityManager.createQuery(query)
					.getResultList();
			return objects;
		} catch (Exception e) {
			throw e;
		}
	}

}
